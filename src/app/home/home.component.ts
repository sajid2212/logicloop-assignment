import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  para = ['Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry']

  likesCount = [57, 57, 57]
  comCount = [6, 6, 6]
  viewsCount = [138, 138, 138]

  constructor() { }

  ngOnInit() {
  }

}
